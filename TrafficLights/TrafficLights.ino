class TrafficLight {
  private:
    int GROUND;
    int GREEN_LED;
    int YELLOW_LED;
    int RED_LED; 
    int YELLOW_TIME;
    int RED_TIME;
    int GREEN_TIME;
    char Name[15];
  
  public:
  TrafficLight(int vcc, int green, int yellow, int red, int cycle, char laneDirection[15]) {
    GROUND = vcc;
    GREEN_LED = green;
    YELLOW_LED = yellow;
    RED_LED = red; 
    RED_TIME = cycle;
    YELLOW_TIME = cycle / 4;
    GREEN_TIME = RED_TIME - YELLOW_TIME;
    for(int i = 0; i < 14; i++) {
      Name[i] = laneDirection[i];
      Name[i + 1] = 0;
    }

    pinMode(GROUND, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
    pinMode(YELLOW_LED, OUTPUT);
    pinMode(RED_LED, OUTPUT);
    digitalWrite(GROUND, LOW); 
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(YELLOW_LED, LOW);
    digitalWrite(RED_LED, LOW);      
  }
  
  void goRunTraffic() {
    while(true) {
      digitalWrite(GREEN_LED, HIGH);
      Serial.print(Name);
      Serial.print(" bound lane has a Green light");
      Serial.println();
      delay(GREEN_TIME);
      digitalWrite(GREEN_LED, LOW);
      digitalWrite(YELLOW_LED, HIGH);
      Serial.print(Name);
      Serial.print(" bound lane has a Yellow light");
      Serial.println();
      delay(YELLOW_TIME);
      digitalWrite(YELLOW_LED, LOW);
      digitalWrite(RED_LED, HIGH);
      Serial.print(Name);
      Serial.print(" bound lane has a Red light");
      Serial.println();
      delay(RED_TIME);
      digitalWrite(RED_LED, LOW);
    }
  }
};

void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  TrafficLight tL(12, 11, 10, 9, 8000, "North");
  tL.goRunTraffic();
}


