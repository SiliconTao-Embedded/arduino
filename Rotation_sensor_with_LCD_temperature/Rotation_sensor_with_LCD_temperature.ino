#include <LiquidCrystal.h>
#include <math.h>
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);


void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(8, 2);

  
}

void loop() {
  int lastVal = 0;
  double lastTemp = 0;
  
  while(1) {
    int newVal = analogRead(0);
    double newTemp = analogRead(1);
    double fenya=(newTemp/1023)*5;
    double r=(5-fenya)/fenya*4700;
    double temperature = 1/(  log(r/10000) /3950 + 1/(25+273.15))-273.15;
    if(newVal != lastVal || newTemp != lastTemp) {
      //lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(newVal);
      lcd.print("   ");
      lcd.setCursor(2,1);
      lcd.print(temperature, 1);
      lcd.print("C");
      lastTemp = newTemp;
      lastVal = newVal;
    }    
    delay(500);  
  }
}


