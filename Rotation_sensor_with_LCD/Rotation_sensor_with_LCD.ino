#include <LiquidCrystal.h>
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);


void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(8, 2);

  
}

void loop() {
  int val = analogRead(0);
  lcd.clear();
  lcd.setCursor(2, 0);
  lcd.print(val);
  delay(1500);  
}


