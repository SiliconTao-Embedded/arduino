#include <LiquidCrystal.h>
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);


class TrafficLight {
  private:
    int GROUND;
    int GREEN_LED;
    int YELLOW_LED;
    int RED_LED; 
    int YELLOW_TIME;
    int RED_TIME;
    int GREEN_TIME;
    char Name[15];
    //char WalkTxt[17] = "  Walk     now";
    //char NoWalkTxt[17] = "  Don't    walk";
    char Walk[8] = "Walk";
    char Dont[8] = "Don't";
    char Now[8] = "Now";
  
  public:
  TrafficLight(int vcc, int green, int yellow, int red, int cycle, char laneDirection[15]) {
    GROUND = vcc;
    GREEN_LED = green;
    YELLOW_LED = yellow;
    RED_LED = red; 
    RED_TIME = cycle;
    YELLOW_TIME = cycle / 4;
    GREEN_TIME = RED_TIME - YELLOW_TIME;
    for(int i = 0; i < 14; i++) {
      Name[i] = laneDirection[i];
      Name[i + 1] = 0;
    }

    pinMode(GROUND, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
    pinMode(YELLOW_LED, OUTPUT);
    pinMode(RED_LED, OUTPUT);
    digitalWrite(GROUND, LOW); 
    digitalWrite(GREEN_LED, LOW);
    digitalWrite(YELLOW_LED, LOW);
    digitalWrite(RED_LED, LOW);      
  }
  
  void goRunTraffic() {
    while(true) {
      Serial.print(Name);
      Serial.print(" bound lane has a Green light");
      Serial.println();
      
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(Walk);
      lcd.setCursor(2, 1);
      lcd.print(Now);
      
      digitalWrite(GREEN_LED, HIGH);
      delay(GREEN_TIME);
      digitalWrite(GREEN_LED, LOW);
      digitalWrite(YELLOW_LED, HIGH);
      lcd.clear();
      for (int i = 20; i > 0; i--) {
        lcd.setCursor(3,0);
        if(i < 10) {
          lcd.print(" ");
        }
        lcd.print(i);
        lcd.print(" ");
        delay(1000);
      }
      
      Serial.print(Name);
      Serial.print(" bound lane has a Yellow light");
      Serial.println();
      
      digitalWrite(YELLOW_LED, LOW);
      Serial.print(Name);
      Serial.print(" bound lane has a Red light");
      Serial.println();
      lcd.clear();
      lcd.setCursor(2, 0);
      lcd.print(Dont);
      lcd.setCursor(2,1);
      lcd.print(Walk);
      digitalWrite(RED_LED, HIGH);      
      delay(RED_TIME);
      digitalWrite(RED_LED, LOW);
    }
  }
};

void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  Serial.begin(9600);

  // set up the LCD's number of columns and rows:
  lcd.begin(8, 2);

  
}

void loop() {
  TrafficLight tL(9, 12, 11, 10, 18000, "North");
  tL.goRunTraffic();
}


