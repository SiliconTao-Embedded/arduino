void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(9, OUTPUT);
  digitalWrite(12, LOW);
  Serial.begin(9600);
}

void loop() {
  boolean pinState = false;
  
  int colorCounter = 0;
  pinState = toggle(pinState, 500, &colorCounter);
  int arrayLength = 180;
  int timerValues[arrayLength];
  for (int counter2 = 0; counter2 < arrayLength; counter2++) {
    timerValues[counter2] = counter2 * 5;
    pinState = toggle(pinState, 100, &colorCounter);
    Serial.print("timerValues[");
    Serial.print(counter2, DEC);
    Serial.print("] = ");
    Serial.print(timerValues[counter2], DEC);
    Serial.println();
  }
  
  for (int positionCount = 0; positionCount < arrayLength; positionCount++) {
    pinState = toggle(pinState, timerValues[positionCount], &colorCounter);  
    Serial.print("next loop ");
    Serial.print(timerValues[positionCount], DEC);
    Serial.println();
  }
  Serial.print("colorCounter = ");
  Serial.print(colorCounter, DEC);
  Serial.println();  
  delay(1000);
}

boolean toggle(boolean newState, int delayTime, int *colorCounter) {

  int level = LOW;
  if(newState == true) { 
    level = HIGH;
    *colorCounter += 1;
  }
  digitalWrite(13, level);
  digitalWrite(11, *colorCounter & 1);
  digitalWrite(10, *colorCounter & 2);
  digitalWrite(9, *colorCounter & 4);

  delay(delayTime);
  return !newState;
}
